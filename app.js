/*********************************************************************/
//////////////BUDGET CONTROLLER////////////////////////////////////////
/*********************************************************************/
var budgetController = (function () {

    var Expense = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
        this.percentage = -1;
    };

    Expense.prototype.calcPercentage = function (totalIncome) { /////// TOTAL INC! here and inside method not used in example
        if (totalIncome > 0) {
            this.percentage = Math.round((this.value / totalIncome) * 100);
        }
        else {
            this.percentage = -1;
        }
    };
    Expense.prototype.getPercentage = function () {
        return this.percentage;
    };

    var Income = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var data = {
        allItems: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        },
        budget: 0,
        percentage: -1
    };

    var calculateTotal = function (type) {
        var sum = 0;
        data.allItems[type].forEach(function (currentItem) {
            sum += currentItem.value;
        });
        data.totals[type] = sum;
    };

    //return object with public methods of budgetController, others methods are hidden
    return {
        addItem: function (type, des, val) {
            var newItem;
            var ID;

            if (data.allItems[type].length >= 1) {
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
            } else {
                ID = 0;
            }

            //create new item depending of a type
            if (type === 'exp') {
                newItem = new Expense(ID, des, val);
            } else if (type === 'inc') {
                newItem = new Income(ID, des, val);
            }

            //push new Element to an appropriete array storage
            data.allItems[type].push(newItem);
            //and return new Element
            return newItem;
        },

        deleteItem: function (type, id) {
            var ids, index;
            console.log('deleteItem type=' + type + " id=" + id);

            //extract IDs from Items array
            ids = data.allItems[type].map(function (currentItem) {
                return currentItem.id;
            });

            //find out index  of ID to delete in allItems[type]
            index = ids.indexOf(id);
            console.log('deleteItem index=' + index);
            if (index !== -1) {
                data.allItems[type].splice(index, 1);
            }
        },

        calculateBudget: function () {

            //calculate total income and expenses
            calculateTotal('inc');
            calculateTotal('exp');
            //calculate the Budget:income-exp
            data.budget = data.totals.inc - data.totals.exp;
            //calc percentage
            if (data.totals.exp > 0 && data.totals.inc > 0) {
                data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
            }
            else {
                data.percentage = -1;
            }
        },

        calculatePercentages: function () {
            data.allItems.exp.forEach(function (currentExpence) {
                currentExpence.calcPercentage(data.totals.inc);
            })
        },

        getPercentages: function () {
            var allPerc = data.allItems.exp.map(function (t) {
                return t.getPercentage();
            });
            return allPerc;
        },

        getCurrentBudget: function () {
            return {
                budget: data.budget,
                inc: data.totals.inc,
                exp: data.totals.exp,
                percentage: data.percentage
            }
        },
        testing: function () {
            return data;
        }
    }
})();


/*********************************************************************/
////////UI CONTROLLER//////////////////////////////////////////////////
/*********************************************************************/

var UIController = (function () {

    //object DOMstrings stores all UI class names in css to call in order to simplify renaming(if it will happen)
    var DOMstrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputButton: '.add__btn',
        incomesContainer: '.income__list',
        expensesContainer: '.expenses__list',
        budgetLabel: '.budget__value',
        incomeLabel: '.budget__income--value',
        expensesLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage',
        container: '.container',
        expensesPercLabel: '.item__percentage',
        dateLabel: '.budget__title--month'
    };

    var formatNumber = function (num, type) {
        // + or - before number
        // XX.00 - two integers after point
        // coma separating thousands
        num = num.toFixed(2);
        var numSplit = num.split('.');
        var int = numSplit[0];
        if (int.length > 3) {
            int = int.substr(0, int.length - 3) + "," + int.substr(int.length - 3)
        }
        var dec = numSplit[1];
        return (type === 'exp' ? '-' : '+') + ' ' + int + '.' + dec;
    };

    // function loop through all elements inside LIST and use CALLBACK function for each element,CALLBACK function could be anyone
    var NodeListForEach = function (list, callback) {
        for (var i = 0; i < list.length; i++) {
            callback(list[i], i);
        }
    };


    return {
        //returns all inputs values
        getInput: function () {
            return {
                type: document.querySelector(DOMstrings.inputType).value, //return: exp or inc
                description: document.querySelector(DOMstrings.inputDescription).value,
                value: parseFloat(document.querySelector(DOMstrings.inputValue).value)
            };
        },

        addListItem: function (obj, type) {
            var html, element;

            //create html string with placeholder text
            if (type === 'inc') {
                element = DOMstrings.incomesContainer;
                html = '<div class="item clearfix" id="inc-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>'
            }
            else if (type === 'exp') {
                element = DOMstrings.expensesContainer;
                html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__percentage">21%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
            }
            //replace placeholder with actual text
            html = html.replace('%id%', obj.id);
            html = html.replace('%description%', obj.description); // it used on example newHtml variable
            html = html.replace('%value%', formatNumber(obj.value, type));
            //insert html into DOM
            document.querySelector(element).insertAdjacentHTML('beforeend', html);
        },

        deleteListItem: function (selectorID) {
            var elem = document.getElementById(selectorID);
            //element could be deleted only from its parent, thats why is used elem.parentNode....
            elem.parentNode.removeChild(elem);
        },

        //clears input fields after enter
        clearFields: function () {
            var fields, fieldsArr;

            //get all fields to be cleared (!note: it returns NodeList - NOT array, should be converted)
            fields = document.querySelectorAll(DOMstrings.inputDescription + ", " + DOMstrings.inputValue);
            //create real Array instead of NodeList
            fieldsArr = Array.prototype.slice.call(fields);
            // make all values empty
            fieldsArr.forEach(function (current, index, array) {
                current.value = '';
            });

            fieldsArr[0].focus();
        },
        //displays budget at the top of page
        displayBudget: function (obj) {
            var type;
            obj.budget > 0 ? type = 'inc' : type = 'exp';

            document.querySelector(DOMstrings.budgetLabel).textContent = formatNumber(obj.budget, type);
            document.querySelector(DOMstrings.incomeLabel).textContent = formatNumber(obj.inc, 'inc');
            document.querySelector(DOMstrings.expensesLabel).textContent = formatNumber(obj.exp, 'exp');


            if (obj.percentage > 0) {
                document.querySelector(DOMstrings.percentageLabel).textContent = obj.percentage + '%';
            }
            else {
                document.querySelector(DOMstrings.percentageLabel).textContent = '--'
            }
        },

        //displays percentages at list of expenses
        displayPercentages: function (percentagesArr) {
            var fields = document.querySelectorAll(DOMstrings.expensesPercLabel);

            NodeListForEach(fields, function (elem, index) {
                if (percentagesArr[index] > 0) {
                    elem.textContent = percentagesArr[index] + '%';
                }
                else {
                    elem.textContent = '--';
                }
            });
        },

        //displays current month+year at the top
        displayMonth: function () {
            var now, year, month, monthsArr;

            now = new Date();
            year = now.getFullYear();
            month = now.getMonth();
            monthsArr = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            document.querySelector(DOMstrings.dateLabel).textContent = monthsArr[month] + " " + year;
        },

        //change colors of input fields into green\red depending on exp or inc type
        changedType: function () {
            var fields = document.querySelectorAll(
                DOMstrings.inputType + ',' +
                DOMstrings.inputDescription + ',' +
                DOMstrings.inputValue
            );
            NodeListForEach(fields, function (currentElement) {
                currentElement.classList.toggle('red-focus'); //should be without a dot '.'
            });
            document.querySelector(DOMstrings.inputButton).classList.toggle('red');
        },

        //return list of DOM elements to use them by other functions
        getDOMstrings: function () {
            return DOMstrings;
        }
    };
})();


/*********************************************************************/
///////////GLOBAL CONTROLLER///////////////////////////////////////////
/*********************************************************************/

var controller = (function (budgetCtrl, UICtrl) {

    //set all event listeners
    var setupEventListeners = function () {
        var DOM = UICtrl.getDOMstrings();

        document.querySelector(DOM.inputButton).addEventListener('click', ctrlAddItem);
        document.addEventListener('keypress', function (event) {
            if (event.keyCode === 13 || event.which === 13) {
                ctrlAddItem();
            }
        });

        document.querySelector(DOM.container).addEventListener('click', ctrlDeleteItem);
        document.querySelector(DOM.inputType).addEventListener('change', UICtrl.changedType);
    };

    var updateBudget = function () {

        // 1. calculate budget
        budgetCtrl.calculateBudget();
        // 2. return new budget
        var budget = budgetCtrl.getCurrentBudget();
        // 3. display new budget
        UICtrl.displayBudget(budget);

    };

    var updatePercentages = function () {

        //1. Calculate %
        budgetCtrl.calculatePercentages();
        //2. read from Budget Controller
        var arrPercentages = budgetCtrl.getPercentages();
        //3. update UI
        UICtrl.displayPercentages(arrPercentages);
    };

    var ctrlAddItem = function () {

        // 1. get the field input data
        var input = UIController.getInput();

        if (input.description !== "" && !isNaN(input.value) && input.value > 0) {
            // 2. add the item to the budget controller
            var newItem = budgetCtrl.addItem(input.type, input.description, input.value)

            // 3. add item to ui
            UICtrl.addListItem(newItem, input.type);

            // 4. clear fields
            UICtrl.clearFields();

            // 5. calculate and update new budget
            updateBudget();

            //6. calculate and update percentages
            updatePercentages();
        }


    };

    var ctrlDeleteItem = function (event) {
        var itemID, splitID, type, ID;

        itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;

        if (itemID) {
            splitID = itemID.split('-');
            type = splitID[0];
            ID = parseInt(splitID[1]);

            //1. delete from data structure
            budgetCtrl.deleteItem(type, ID);

            //2. delete from UI
            UICtrl.deleteListItem(itemID);

            //3. update and show new budget
            updateBudget();

            //4. calculate and update percentages
            updatePercentages();
        }

    };


    return {
        //function for script initialization
        init: function () {
            console.log('init...');
            setupEventListeners();
            UICtrl.displayMonth();
            UICtrl.displayBudget({
                budget: 0,
                inc: 0,
                exp: 0,
                percentage: 0
            });
        }
    }

})(budgetController, UIController);

//launch script
controller.init();